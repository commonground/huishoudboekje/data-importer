import argparse
import json
from dotenv import load_dotenv

from inserter import Inserter
from generator import Generator
from validator import JsonInputValidator

title = 'HHB Data importer 2.0.4'

def load_data(file_path):
    with open(file_path) as f:
        return json.load(f)
    
def insert(file_path, only_validate, seed):
    input_data = load_data(file_path)
    validator = JsonInputValidator()
    validator.validate(input_data)

    if not only_validate:
        inserter = Inserter(seed)
        inserter.insert(input_data)

def generate(seed):
    default_data = load_data('data/default/data.json')
    generator = Generator(default_data, seed=seed)
    generator.generate()

def main():
    parser = argparse.ArgumentParser(description=title)
    subparsers = parser.add_subparsers(dest='command', required=True)
    
    # Parser for the "insert" command
    parser_insert = subparsers.add_parser('insert', help='Insert data from a json file')
    parser_insert.add_argument('file', type=str, help='Path to the json file')
    parser_insert.add_argument('--only-validate', action='store_true', help='Only validate the data, it wont be inserted')
    parser_insert.add_argument('--seed', type=int, help='Seed for uuid generation')
    
    # Parser for the "generate" command
    parser_generate = subparsers.add_parser('generate', help='Generate random data')
    parser_generate.add_argument('--seed', type=int, help='Seed for data generation')

    print("Starting " + title)


    args = parser.parse_args()

    seed = args.seed
    if seed is None:
        seed = sum(ord(char) for char in "huishoudboekje")

    if args.command == 'insert':
        insert(args.file, args.only_validate, seed)

    elif args.command == 'generate':
        generate(seed)

    print('Finished')

if __name__ == "__main__":
    load_dotenv()
    main()