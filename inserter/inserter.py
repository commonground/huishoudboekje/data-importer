import json
import os
import uuid
from faker import Faker
import sqlalchemy as sa
from sqlalchemy import text

class Inserter:

    def __init__(self, seed=123456789):
        self.fake = Faker('nl_NL')
        Faker.seed(seed)

    def insert(self, input_data):
        party_engine = sa.create_engine(os.environ.get("DB_PARTYSERVICE_URL"))
        hhb_engine = sa.create_engine(os.environ.get("DB_HUISHOUDBOEKJESERVICE_URL"))
        with party_engine.begin() as party_conn:
            with hhb_engine.begin() as hhb_conn:
                print("inserting configurations")
                self.__insert_configuration(input_data["configurations"], hhb_conn)
                print("inserting categories")
                self.__insert_categories(input_data["categories"], hhb_conn)
                print("inserting organisaties")
                self.__insert_organisations(input_data["organisations"], party_conn)
                print("inserting civilians")
                self.__insert_civilians(input_data["civilians"], party_conn, hhb_conn)
                print("updating seq tables")
                self.__update_seq_tables(hhb_conn)
            
                party_conn.commit()
                hhb_conn.commit()


    def __update_seq_tables(self, hhb_conn):
        update_stmt = text("""
                SELECT setval('rubrieken_id_seq', (SELECT MAX(id) FROM rubrieken), true);
            """)
        hhb_conn.execute(update_stmt)

    def __insert_civilians(self, civilians, party_conn, hhb_conn):
        insert_data_civilians = []
        insert_data_agreements = []
        insert_data_rekeningen = []
        insert_data_rekeningen_burger = []
        insert_data_address = []
        insert_data_huishouden = []
        huisouden_ids = set()

        
        for civilian in civilians:

            if civilian["huishouden_uuid"] not in huisouden_ids:
                huisouden_ids.add(civilian["huishouden_uuid"])
                insert_data_huishouden.append({
                    "uuid": civilian["huishouden_uuid"],
                })

            address_uuid = uuid.uuid4()

            insert_data_civilians.append({
                "uuid": civilian["uuid"],
                "huishouden_id": civilian["huishouden_uuid"],
                "bsn": civilian["bsn"],
                "hhb_number": civilian["hhb_number"],
                "infix": "",
                "voorletters": civilian["voorletters"],
                "voornamen": civilian["voornamen"],
                "achternaam": civilian["achternaam"],
                "telefoonnummer": civilian["telefoonnummer"],
                "email": civilian["email"],
                "end_date": civilian["end_date"],
                "start_date": civilian["start_date"],
                "saldo_alarm": True,
                "address_uuid": address_uuid,
                "geboortedatum": civilian["geboortedatum"]})

            insert_data_address.append({
                "uuid": address_uuid,
                "straatnaam": civilian["straatnaam"],
                "huisnummer": civilian["huisnummer"],
                "postcode": civilian["postcode"],
                "plaatsnaam": civilian["plaatsnaam"]})
            
            rekeningen = civilian["rekeningen"]

            for rekening in rekeningen:
                insert_data_rekeningen_burger.append({
                    "rekening": rekening["uuid"],
                    "burger": civilian["uuid"]
                })
                insert_data_rekeningen.append({
                    "uuid": rekening["uuid"],
                    "rekeninghouder": rekening["rekeninghouder"],
                    "iban": rekening["iban"]
                })
            
            agreements = civilian["agreements"]

            for agreement in agreements:
                insert_data_agreements.append({
                    "uuid": self.fake.uuid4(),
                    "burger_uuid": civilian["uuid"],
                    "omschrijving": agreement["omschrijving"],
                    "bedrag": agreement["bedrag"],
                    "credit": agreement["credit"],
                    "valid_from": agreement["valid_from"],
                    "valid_through": agreement["valid_through"],
                    "zoektermen": agreement["zoektermen"],
                    "rekening_uuid": agreement["rekening_uuid"],
                    "rubriek_id": agreement["rubriek_id"],
                    "postadres_id": agreement["postadres_id"],
                    "afdeling_uuid": agreement["afdeling_uuid"],
                })

        if len(insert_data_huishouden) > 0:
            insert_huishouden_stmt = text("""INSERT INTO households (uuid) VALUES
                                    (:uuid)""")
            party_conn.execute(insert_huishouden_stmt, insert_data_huishouden)


        if len(insert_data_address) > 0:
            insert_addresses_stmt = text("""INSERT INTO addresses (uuid, street, house_number, postal_code, city) VALUES
                                    (:uuid, :straatnaam, :huisnummer, :postcode, :plaatsnaam )""")
            party_conn.execute(insert_addresses_stmt, insert_data_address)
            
        if len(insert_data_civilians) > 0:
            insert_civilians_stmt = text("""INSERT INTO citizens (uuid, hhb_number, phone_number, email, birth_date, surname, infix,
                                         initials, first_names, household_id, bsn, use_saldo_alarm, end_date, start_date, address_id) VALUES
                                    (:uuid, :hhb_number, :telefoonnummer, :email, :geboortedatum, :achternaam, :infix,
                                        :voorletters, :voornamen, :huishouden_id, :bsn, :saldo_alarm, :end_date, :start_date, :address_uuid)""")
            party_conn.execute(insert_civilians_stmt, insert_data_civilians)

        if len(insert_data_rekeningen) > 0:
            insert_rekeningen_stmt = text("""INSERT INTO accounts (uuid, iban, account_holder) VALUES
                                    (:uuid, :iban, :rekeninghouder)""")
            party_conn.execute(insert_rekeningen_stmt, insert_data_rekeningen)

        if len(insert_data_rekeningen_burger) > 0:
            insert_rekeningen__burger_stmt = text("""INSERT INTO citizen_accounts (account_id, citizen_id) VALUES
                                    (:rekening, :burger)""")
            party_conn.execute(insert_rekeningen__burger_stmt, insert_data_rekeningen_burger)

        if len(insert_data_agreements) > 0:
            insert_agreements_stmt = text("""INSERT INTO afspraken (uuid, burger_uuid, valid_from, valid_through, tegen_rekening_uuid, bedrag, credit, omschrijving,
                                         rubriek_id, zoektermen, postadres_id, afdeling_uuid) VALUES
                                    (:uuid, :burger_uuid, :valid_from, :valid_through, :rekening_uuid, :bedrag, :credit, :omschrijving, :rubriek_id,
                                        :zoektermen, :postadres_id, :afdeling_uuid)""")
            hhb_conn.execute(insert_agreements_stmt, insert_data_agreements)


    def __insert_categories(self, categories, hhb_conn):
        insert_data = []
        
        for category in categories:

            insert_data.append({
                "id": category["id"],
                "naam": category["naam"],
                "grootboekrekening_id": category["grootboekrekening_id"],
                "uuid": self.fake.uuid4()
            })

        if len(insert_data) > 0:
            insert_stmt = text("""INSERT INTO rubrieken (id, naam, grootboekrekening_id, uuid) VALUES
                                    (:id, :naam, :grootboekrekening_id, :uuid)""")
            hhb_conn.execute(insert_stmt, insert_data)
    
    def __insert_configuration(self, configurations, hhb_conn):
        insert_data = []
        
        for configuration in configurations:

            insert_data.append({
                "id": configuration["id"],
                "value": configuration["value"],
            })

        if len(insert_data) > 0:
            insert_stmt = text("""INSERT INTO configuratie (id, waarde) VALUES
                                    (:id, :value)""")
            hhb_conn.execute(insert_stmt, insert_data)


    def __insert_organisations(self, organisations, party_conn):
        insert_data_organisations = []
        insert_data_departments = []
        insert_data_addresses = []
        insert_data_department_address = []
        insert_data_accounts = []
        insert_data_department_account = []

        for organisation in organisations:

            insert_data_organisations.append({
                "uuid": organisation["uuid"],
                "name": organisation["name"],
                "kvk": organisation["kvkNumber"],
                "branch": organisation["branchNumber"]
            })

            departments = organisation["departments"]

            for department in departments:
                postaddresses = department["addresses"]
                accounts = department["accounts"]

                for address in postaddresses:
                    insert_data_addresses.append({
                        "uuid": address["uuid"],
                        "straatnaam": address["straatnaam"],
                        "huisnummer": address["huisnummer"],
                        "postcode": address["postcode"],
                        "plaatsnaam": address["plaatsnaam"]
                    })
                    insert_data_department_address.append({
                        "address_uuid": address["uuid"],
                        "department_uuid": department["uuid"],
                    })

                for account in accounts:
                    insert_data_accounts.append({
                        "uuid": account["uuid"],
                        "rekeninghouder": account["rekeninghouder"],
                        "iban": account["iban"]
                    })
                    insert_data_department_account.append({
                        "account_uuid": account["uuid"],
                        "department_uuid": department["uuid"]
                    })

                insert_data_departments.append({
                    "uuid": department["uuid"],
                    "name": department["name"],
                    "postadressen": [],
                    "rekeningen" : [],
                    "organisation": organisation["uuid"]
                })


        if len(insert_data_organisations) > 0:
            insert_organisations_stmt = text(
                "INSERT INTO organisations (uuid, name, kvk_number, branch_number) VALUES (:uuid, :name, :kvk, :branch)")
            party_conn.execute(insert_organisations_stmt, insert_data_organisations)

        if len(insert_data_departments) > 0:
            insert_departments_stmt = text("""INSERT INTO departments (uuid, name, postadressen_ids, rekeningen_ids, organisation_uuid) VALUES
                                    (:uuid, :name, :postadressen, :rekeningen, :organisation)""")
            party_conn.execute(insert_departments_stmt, insert_data_departments)

        if len(insert_data_addresses) > 0:
            insert_addresses_stmt = text("""INSERT INTO addresses (uuid, street, house_number, postal_code, city) VALUES
                                    (:uuid, :straatnaam, :huisnummer, :postcode, :plaatsnaam)""")
            party_conn.execute(insert_addresses_stmt, insert_data_addresses)

        if len(insert_data_accounts) > 0:
            insert_rekeningen_stmt = text("""INSERT INTO accounts (uuid, iban, account_holder) VALUES
                                    (:uuid, :iban, :rekeninghouder)""")
            party_conn.execute(insert_rekeningen_stmt, insert_data_accounts)

        if len(insert_data_department_account) > 0:
            insert_department_account_stmt = text("""INSERT INTO department_accounts (account_id, department_id) VALUES
                                    (:account_uuid, :department_uuid)""")
            party_conn.execute(insert_department_account_stmt, insert_data_department_account)

        if len(insert_data_department_address) > 0:
            insert_department_address_stmt = text("""INSERT INTO department_addresses (address_uuid, department_uuid) VALUES
                                    (:address_uuid, :department_uuid)""")
            party_conn.execute(insert_department_address_stmt, insert_data_department_address)
    