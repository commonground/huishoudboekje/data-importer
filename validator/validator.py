import json
import os
import jsonschema
from jsonschema import Draft7Validator, RefResolver

class JsonInputValidator:
    
    def __init__(self):
        self.file_path = 'validator/schemas/base.schema.json'

    
    def load_schema(self, file_path):
        with open(file_path) as f:
            return json.load(f)
        
    def get_schema_store(self, base_schema):
        account_schema = self.load_schema('validator/schemas/account.schema.json')
        address_schema = self.load_schema('validator/schemas/address.schema.json')
        agreement_schema = self.load_schema('validator/schemas/agreement.schema.json')
        category_schema = self.load_schema('validator/schemas/category.schema.json')
        civilian_schema = self.load_schema('validator/schemas/civilian.schema.json')
        configuration_schema = self.load_schema('validator/schemas/configuration.schema.json')
        department_schema = self.load_schema('validator/schemas/department.schema.json')
        organisation_schema = self.load_schema('validator/schemas/organisation.schema.json')
        return {
            base_schema['$id'] : base_schema,
            account_schema['$id'] : account_schema,
            address_schema['$id'] : address_schema,
            agreement_schema['$id'] : agreement_schema,
            category_schema['$id'] : category_schema,
            civilian_schema['$id'] : civilian_schema,
            configuration_schema['$id'] : configuration_schema,
            department_schema['$id'] : department_schema,
            organisation_schema['$id'] : organisation_schema,
        }
                
    def validate(self, input):
        base_schema = self.load_schema(self.file_path)
        resolver = RefResolver.from_schema(base_schema, store=self.get_schema_store(base_schema))
        validator = Draft7Validator(base_schema, resolver=resolver)
        #try:
        validator.validate(input)
        print("Data is valid")
        # except jsonschema.exceptions.ValidationError as err:
        #     print("Data is invalid")
        #     print(err.message)