jsonschema>=4.9.0,<5
Faker==29.0.0
python-dotenv==1.0.0
SQLAlchemy>=2.0.22,<3
psycopg2-binary>=2.9.6,<3
pytz==2024.2