# Huishoudboekje Data Importer

This package will insert data into it a huishoudboekje database.

⚠️ Please use this tool at your own risk! This application is not designed to run in a production environment. 
We are currently just using it in our own CI and on our own local development environments as it's honestly too 
expensive to have us sitting there inputting the same data all the time when the robots can do it for us. 😄

### Data

You can find example data in JSON format in `./data/example/data.json`. This is exactly what gets loaded into the API.

## Usage

The following environment variables are used:



```shell
# The connection URL of party service db
DB_PARTYSERVICE_URL

# The connection URL of postadressen service db
DB_POSTADRESSENSERVICE_URL

# The connection URL of hhb service db
DB_HUISHOUDBOEKJESERVICE_URL

# Optionally for generating you can enter the amount of organisations that need to be created
NUM_ORGANISATIONS

# Optionally for generating you can enter the amount of civilians that need to be created
NUM_CIVILIANS
```

### Docker

You can use our prebuilt Docker image to run this application. You can get it from our registry at 
`registry.gitlab.com/commonground/huishoudboekje/data-importer/data-importer`

## Commands

```shell
python app.py generate

#To add a a custom seed for generating
python app.py generate --seed=1234
```


```shell

#Generated data will be stored in data/generated/data.json but any path to a valid data file can be provided here
python app.py insert data/generated/data.json

#If you just want to validate the data use the --only-validate flag
python app.py insert data/generated/data.json --only-validate
```

