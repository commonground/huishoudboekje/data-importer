import json
import os
import random
from faker import Faker

from generator.civilian import CivilianGenerator
from generator.counter import SimpleCounter
from generator.custom_name_provider import CustomNameProvider
from generator.organisations import OrganisationGenerator

class Generator:


    def __init__(self, default_data, seed=0):
        self.fake = Faker('nl_NL')
        self.fake.add_provider(CustomNameProvider)

        self.default_data = default_data
        Faker.seed(seed)
        random.seed(seed)

    def generate(self):
        print("generating data")
        num_organisations = int(os.environ.get("NUM_ORGANISATIONS", "200"))
        organisation_generator = OrganisationGenerator(self.fake)
        organisations = [organisation_generator.generate() for _ in range(num_organisations)]

        num_civilians = int(os.environ.get("NUM_CIVILIANS", "500"))
        civilian_generator = CivilianGenerator(self.fake, organisations, self.default_data['categories'])
        civilians = [civilian_generator.generate() for _ in range(num_civilians)]
        
        print("saving data to data/generated/data.json")
        generated = self.default_data.copy()
        generated["organisations"].extend(organisations)
        generated["civilians"].extend(civilians)
        self.__save_json_to_file(generated)

    
    def __save_json_to_file(self, data):
        folder_path = 'data/generated'
        file_name = 'data.json'
        file_path = os.path.join(folder_path, file_name)
        
        os.makedirs(folder_path, exist_ok=True)

        with open(file_path, 'w') as json_file:
            json.dump(data, json_file, indent=4)