from datetime import datetime
import os
from faker import Faker
import random

class AgreementGenerator:

    uitgaven_normal = [
        "Huishuur",
        "Hypotheekbetaling",
        "Gasrekening",
        "Elektriciteitsrekening",
        "Waterrekening",
        "Internet en telefoonrekening",
        "Autoverzekering",
        "Autolease of huur",
        "Benzine of brandstofkosten",
        "Boodschappen",
        "Zorgverzekering",
        "Onderhoudskosten woning",
        "Gemeentebelasting",
        "Onderwijs- en studiekosten",
        "Sport- en fitnessabonnementen",
        "Vakanties en reizen",
        "Kleding en schoenen",
        "Entertainment en streamingdiensten",
        "Restaurant en uit eten",
        "Reparatiekosten auto"
    ]

    uitgaven_fastfood = [
        "Huishuur",
        "Hypotheekbetaling",
        "Gasrekening",
        "Kosten voor dagelijkse koffie van Starbucks",
        "Elektriciteitsrekening",
        "Waterrekening",
        "McDonald's Happy Meal kosten",
        "Autolease of huur voor fastfoodleveringen",
        "Bestelling van een KFC bucket",
        "Boodschappen",
        "Zorgverzekering",
        "Onderhoudskosten woning",
        "Kosten voor een Double Whopper bij Burger King",
        "Reclame-uitgaven voor fastfoodbedrijf",
        "Uitgaven voor Uber Eats bezorging",
        "Kleding en schoenen",
        "Kosten voor bonus milkshake bij Wendy's",
        "Restaurantbezoeken",
        "Voertuigkosten voor het bezorgen van fastfood",
        "Extra sausjes en bijgerechten van Taco Bell"
    ]

    inkomsten_normal =  [
        "Salaris",
        "Freelance inkomen",
        "Bijstandsuitkering",
        "Kinderbijslag",
        "Huurinkomsten",
        "Rente op spaargeld",
        "Dividenduitkering",
        "Giften",
        "Overwerkvergoeding",
        "Pensioenuitkering",
        "Bonussen",
        "Partner inkomen",
        "Opbrengsten uit investeringen",
        "Commissies",
        "Verkoop van activa",
        "Aandeelhoudersuitkering",
        "Erfenissen",
        "Vakantiegeld",
        "Bijverdiensten (bijbaan)",
        "Loterij of prijzengeld"
    ]

    inkomsten_fastfood = [
        "Salaris",
        "Freelance inkomen",
        "Bonussen van McDonald's verkoop",
        "Opbrengsten uit Burger King franchise",
        "Kinderbijslag",
        "Huurinkomsten",
        "Rente op spaargeld",
        "Verkoop van Happy Meals",
        "Overwerkvergoeding bij Domino's Pizza",
        "Commissies op Uber Eats leveringen",
        "Pensioenuitkering",
        "Partner inkomen",
        "Inkomsten uit bonus milkshake promotie bij Wendy's",
        "Vakantiegeld",
        "Opbrengst van een fastfood food truck",
        "Franchise-inkomsten van een Taco Bell",
        "Verkoop van McFlurry's in promotie",
        "Extra inkomsten uit fastfoodcombo-aanbiedingen",
        "Bijverdiensten van een bijbaan bij Subway",
        "Verkoop van KFC maaltijddeals"
    ]

    def __init__(self, fake: Faker, organisations, categories):
        self.ENABLE_FASTFOOD_MODE = int(os.environ.get("ENABLE_FASTFOOD_MODE", "0")) > 0
        self.fake = fake
        self.categories = [item for item in categories if item["id"] != 11] # Very specific Prive opname category in default data
        self.organisations = organisations

        if self.ENABLE_FASTFOOD_MODE:
            self.inkomsten = self.inkomsten_fastfood
            self.uitgaven = self.uitgaven_fastfood
        else:
            self.inkomsten = self.inkomsten_normal
            self.uitgaven = self.uitgaven_normal

    def generate(self, burger_hhb_number: str, burger_bsn: str, start_date:str, burger_rekening_uuid: str, burger_agreement: bool):
        zoektermen = [burger_hhb_number, burger_bsn]
        if burger_agreement:
            postadres_id = None
            afdeling_uuid = None
            rekening_uuid = burger_rekening_uuid
            rubriek_id = 11 # Very specific Prive opname category in default data
            credit = False
            omschrijving = "Leefgeld"
        else:
            postadres_id, afdeling_uuid, rekening_uuid = self.__get_party()
            rubriek_id, credit = self.__get_category_credit()
            omschrijving = self.__generate_omschrijving(credit)

        zoektermen.append(omschrijving)

        return {
            "omschrijving": omschrijving,
            "bedrag": random.randint(1, 1000) * 100,
            "credit": credit,
            "valid_from": self.__generate_start_date(start_date),
            "valid_through": None,
            "zoektermen": zoektermen,
            "rekening_uuid": rekening_uuid,
            "rubriek_id": rubriek_id,
            "postadres_id": postadres_id,
            "afdeling_uuid": afdeling_uuid
        }
    
    def __get_party(self):
        organisation =  random.choice(self.organisations)
        department = random.choice(organisation["departments"])
        address = random.choice(department["addresses"])
        account = random.choice(department["accounts"])
        return address["uuid"], department["uuid"], account["uuid"]
    
    def __get_category_credit(self):
        category = random.choice(self.categories)
        return category["id"], category["credit"]
    
    def __generate_omschrijving(self, credit):
        if credit:
            return random.choice(self.inkomsten)
        return random.choice(self.uitgaven)
    
    def __generate_start_date(self, unix_start_date):
        start_date = datetime.fromtimestamp(unix_start_date)
        end_date = datetime(2023, 12, 31)
        random_date = self.fake.date_between_dates(date_start=start_date, date_end=end_date)
        return random_date.strftime('%Y-%m-%d %H:%M:%S')