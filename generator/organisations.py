from datetime import datetime
from faker import Faker
import random

from generator.counter import SimpleCounter
from generator.department import DepartmentGenerator

class OrganisationGenerator:
    def __init__(self, fake: Faker):
        self.fake = fake
        self.generated_combinations = set()
        self.department_generator = DepartmentGenerator(fake)

    def generate(self):
        name = self.__format_string(self.fake.company())
        kvk, branch = self.__generate_unique_numbers()
        return {
            "uuid": self.fake.uuid4(),
            "name": name,
            "kvkNumber": kvk,
            "branchNumber": branch,
            "departments": self.__generate_departments(name)
        }
    
    def __generate_unique_numbers(self):
        while True:
            kvk = ''.join(random.choices('0123456789', k=8))
            branch = ''.join(random.choices('0123456789', k=12))
            combination = (kvk, branch)

            if combination not in self.generated_combinations:
                self.generated_combinations.add(combination)
                return combination
            
    def __generate_departments(self, name):
        num_depertments = random.randint(2, 5)
        return [self.department_generator.generate(num, name) for num in range(num_depertments)]
    
    def __format_string(self, input_string):
        if len(input_string) > 100:
            return input_string[:97] + '...'
        return input_string
  