from faker import Faker
from faker.providers import BaseProvider


class CustomNameProvider(BaseProvider):
    # Custom first names
    def custom_first_name(self):
        first_names = [
            # Single-word first names
            "Patty", "Cheddar", "Mac", "Grill", "Bacon", "Fry", "Pickle", "Onion", 
            "Sauce", "Nugget", "BBQ", "Jalapeño", "Ketchup", "Crispy", "Cheese", 
            "Guac", "Sriracha", "Buffalo", "Beefy", "Chili", "Mayo", "Lettuce", 
            "Tomato", "Sesame", "Angus", "Fries", "Ranch", "Swiss", "Mushroom", 
            "Pepper", "Chicken", "Egg", "Mustard", "Turkey", "Chipotle", "Avocado", 
            "Whopper", "Tater", "Smash", "Crisps", "Bun", "Zesty", "Barbecue", 
            "Crunch", "Deluxe", "Flame", "Sweet", "Tangy", "Stack", "Ultimate", 
            "Golden", "Classic", "Tender", "Melty", "Veggie", "Tasty", "Spicy",
            "Beef", "Bison", "Pork", "Lentil","Quinoa",  "Fish", "Wagyu", "Falafel",
            "Crab","Blackbean", "Tofu", "Venison", "Salmon"
            
            # Two-word first names
            "Patty Melt", "Cheddar Jack", "Big Mac", "Bacon Bliss", "Fry King", 
            "Double Cheese", "Smokey BBQ", "Spicy Jalapeño", "Ketchup Kat", 
            "Crispy Bacon", "Cheese Lover", "Guac Attack", "Buffalo Blaze", 
            "Beefy Deluxe", "Mayo Magic", "Sweet BBQ", "Ultimate Bacon", 
            "Grilled Chicken", "Triple Stack", "Loaded Fries", "Zesty Ranch", 
            "Swiss Melt", "Pepper Jack", "Tangy Mustard", "Buffalo Ranch", 
            "Avocado Smash", "Cheddar Delight", "Crispy Onions", "Big Crunch", 
            "Barbecue Crunch", "Loaded Tater", "Sriracha Heat", "Golden Fries", 
            "Ranch Lover", "Chicken Nugget", "Angus Beef", "Grass-Fed Beef",
            "Wagyu Beef", "Ground Turkey", "Chicken Breast", "Veggie Patty", 
            "Black Bean Patty","Lentil Patty","Bison Patty", "Pork Patty",
            "Mushroom Patty","Quinoa Patty", "Falafel Patty", "Salmon Patty",
            "Impossible Burger",

            # Three-word first names
            "Bacon Cheese Deluxe", "Ultimate Chicken Nugget", "Crispy Bacon Fries",
            "Grilled Onion Melt", "Cheddar Ranch Supreme", "Sweet BBQ Crunch", 
            "Double Bacon Stack", "Loaded Cheese Fries", "Spicy Chicken Deluxe",
            "Triple Patty Melt", "Crispy Chicken Patty", "Spicy Chicken Patty"
        ]
        return self.random_element(first_names)
    
    # Custom last names
    def custom_last_name(self):
        last_names = [
            "Bunson", "McDouble", "Whopperton", "Frizzle", "Sliderstein",
            "Griddle", "Picklesworth", "McFlurry", "Fryman", "Tenders",
            "Wrapstein", "Greasewood", "Pepperjack", "Onionsmith", "Shakester",
            "Crispington", "Baconshire", "Cheeseworth", "Sesamefield", "Charbroil",
            "McGrill", "Sizzleton", "Greaseworthy", "Cheddarsmith", "Beefington",
            "Saucewell", "Fryston", "Crispywood", "Bunworthy", "Munchson",
            "Toppingsley", "Flavourstein", "Grillford", "Bunsley", "Saucington",
            "Chompstein", "Nuggetson", "McCheese", "Frywood", "Spiceburg",
            "Toastwell", "Pickleson", "Crumblesworth", "Lettuceford", "Ketchum",
            "Sizzleworth", "Tastingson", "Dippington", "Crispley", "Grillsworth",
            "Sesameson", "Whopperly", "Mayoington", "Drizzleton", "Jalapenoford"
        ]
        return self.random_element(last_names)