from datetime import datetime
import os
from faker import Faker
import random
import pytz

from generator.account import AccountGenerator
from generator.agreement import AgreementGenerator
from generator.counter import SimpleCounter

class CivilianGenerator:

    
    def __init__(self, fake: Faker, organisations, categories):
        self.ENABLE_FASTFOOD_MODE = int(os.environ.get("ENABLE_FASTFOOD_MODE", "0")) > 0
        self.fake = fake
        self.households = []
        self.account_generator = AccountGenerator(fake)
        self.agreement_generator = AgreementGenerator(fake, organisations, categories)

    def generate(self):
        if self.ENABLE_FASTFOOD_MODE:
            voornamen = self.fake.custom_first_name()
            achternaam = self.fake.custom_last_name()
        else:
            voornamen = self.fake.first_name()
            achternaam = self.fake.last_name()

        voorletters = self.__extract_initials(voornamen)
        bsn = self.fake.ssn()
        start_date = self.__generate_start_date()
        rekeningen = self.__generate_accounts(voorletters + ' ' + achternaam)
        hhb_number =  self.__generate_hhb_number()

        return {
            "uuid": self.fake.uuid4(),
            "hhb_number":hhb_number, 
            "huishouden_uuid": self.__generate_or_get_household(),
            "bsn": bsn,
            "voorletters": voorletters,
            "voornamen": voornamen,
            "achternaam": achternaam,
            "telefoonnummer": self.__generate_phonenumber() if self.__should_generate() else None,
            "email": self.fake.email() if self.__should_generate() else None,
            "geboortedatum": self.__fake_birthday(),
            "end_date": None,
            "start_date": start_date,
            "straatnaam": self.fake.street_name(), 
            "huisnummer": self.fake.building_number(),
            "postcode": self.fake.postcode().replace(" ", ""),
            "plaatsnaam": self.fake.city(),
            "rekeningen": rekeningen,
            "agreements": self.__generate_agreements(hhb_number, bsn, start_date, rekeningen[0]["uuid"])
        }
    
    def __extract_initials(self, name):
        parts = name.split()
        initials = '.'.join([part[0].upper() for part in parts]) + '.' 
        return initials
    
    def __generate_hhb_number(self):
        return "HHB" + str(random.randint(1000000, 9999999))
    
    def __generate_or_get_household(self):
        if len(self.households) > 0 and random.random() < 0.1:
            return random.choice(self.households)
        else:
            new_houshold = self.fake.uuid4()
            self.households.append(new_houshold)
            return new_houshold

    
    def __fake_birthday(self):
        dob = self.fake.date_of_birth()  
        string_date = dob.strftime('%Y-%m-%d 00:00:00')
        datetime_date = datetime.strptime(string_date,'%Y-%m-%d 00:00:00')
        amsterdam_tz = pytz.timezone('Europe/Amsterdam')
        localized_datetime = amsterdam_tz.localize(datetime_date)
        return int(datetime.timestamp(localized_datetime))
    
    def __should_generate(self):
        return random.random() < 0.8
    
    def __generate_start_date(self):
        start_date = datetime(2019, 1, 1)
        end_date = datetime(2022, 12, 31)
        random_date = self.fake.date_between_dates(date_start=start_date, date_end=end_date)
        string_date = random_date.strftime("%d/%m/%Y")
        datetime_date = datetime.strptime(string_date,"%d/%m/%Y")
        return int(datetime.timestamp(datetime_date))

    def __generate_accounts(self, name):
        num_accounts = random.randint(1, 2)
        return [self.__generate_account(name) for _ in range(num_accounts)]
    
    def __generate_account(self, name):
        return self.account_generator.generate(name=name)
    

    def __generate_agreements(self, hhb_number: str, burger_bsn: str, start_date:str , burger_rekening_id: str):
        num_agreements = random.randint(1, 5)
        agreements =  [self.__generate_agreement(hhb_number, burger_bsn, start_date, -1, False) for _ in range(num_agreements)]
        agreements.append(self.__generate_agreement(hhb_number, burger_bsn, start_date, burger_rekening_id, True))
        return agreements
    
    def __generate_agreement(self, hhb_number: str, burger_bsn: str, start_date:str , burger_rekening_id: str, burger_agreement: bool):
        return self.agreement_generator.generate(hhb_number, burger_bsn, start_date, burger_rekening_id, burger_agreement)
    

    def __generate_phonenumber(self):
        return '06' + ''.join([str(random.randint(1 if num == 0 else 0, 9)) for num in range(8)])

