from datetime import datetime
import os
from faker import Faker
import random

from generator.account import AccountGenerator
from generator.counter import SimpleCounter

class DepartmentGenerator:

    department_names_normal = [
        "Personeelszaken (HR)",
        "Informatie Technologie (IT)",
        "Marketing en Communicatie",
        "Financiën en Boekhouding",
        "Onderzoek en Ontwikkeling (R&D)",
        "Klantenservice",
        "Verkoop en Business Development",
        "Juridische Zaken en Compliance",
        "Operaties en Logistiek",
        "Productbeheer",
        "Kwaliteitsborging (QA)",
        "Supply Chain Management",
        "Public Relations (PR)",
        "Bedrijfsstrategie",
        "Faciliteiten en Onderhoud",
        "Risicomanagement",
        "Data-analyse en Inzichten",
        "Training en Ontwikkeling",
        "Inkoop en Aankoop",
        "Innovatie en Strategie",
        "Interne Audit",
        "Content en Mediaproductie",
        "Business Intelligence",
        "Maatschappelijk Verantwoord Ondernemen (MVO)",
        "Beveiliging en Toezicht",
        "Technische Dienst en Engineering",
        "Partnerschappen en Allianties",
        "Milieu, Gezondheid en Veiligheid (EHS)",
        "Klantenervaring (CX)",
        "Diversiteit, Gelijkheid en Inclusie (DEI)"
    ]

    department_names_fastood = [
        "Grillafdeling",
        "Burgerbereiding",
        "Frituurafdeling",
        "Saus- en Specerijenbeheer",
        "Drankenafdeling",
        "Toetjes en IJscreaties",
        "Broodjes en Buns Beheer",
        "Sla en Groentevoorbereiding",
        "Keukenbeheer",
        "Kwaliteitscontrole",
        "Voedselveiligheid",
        "Voorraden en Voorbereiding",
        "Klantenservice",
        "Drive-Thru Beheer",
        "Voedingslogistiek",
        "Menuontwikkeling",
        "Marketing en Promoties",
        "Voedingswaarden en Analyse",
        "Franchisebeheer",
        "Interieur en Sfeerbeheer",
        "Leveranciersbeheer",
        "Personeelsbeheer",
        "Klachtenafhandeling",
        "Interne Training en Opleiding",
        "Fastfood Innovatie",
        "Uitgifte en Verpakking",
        "Productontwerp en Proefkeuken",
        "Kassa en Betaling",
        "Sociale Media en Reclame",
        "Evenementenbeheer",
        "Voedselverspilling en Recycling"
    ]

    def __init__(self, fake: Faker):
        self.ENABLE_FASTFOOD_MODE = int(os.environ.get("ENABLE_FASTFOOD_MODE", "0")) > 0
        self.fake = fake
        self.account_generator = AccountGenerator(fake)
        if self.ENABLE_FASTFOOD_MODE:
            self.department_names = self.department_names_fastood
        else:
            self.department_names = self.department_names_normal


    def generate(self, num, organisation_name):
        name = self.__get_name(num, organisation_name)
        return {
            "uuid": self.fake.uuid4(),
            "name": name,
            "addresses": self.__generate_addresses(),
            "accounts":  self.__generate_accounts(organisation_name + ' ' + name)
        }
    
    def __get_name(self, num, name):
        if num == 1:
            return name
        return random.choice(self.department_names)
    
    def __generate_accounts(self, name):
        num_accounts = random.randint(1, 3)
        return [self.__generate_account(name) for _ in range(num_accounts)]
    
    def __generate_account(self, name):
        return self.account_generator.generate(name=name)
    

    def __generate_addresses(self):
        num_addresses = random.randint(1, 3)
        return [self.__generate_address() for _ in range(num_addresses)]
    
    def __generate_address(self):
        return {
            "uuid": self.fake.uuid4(),
            "straatnaam": self.fake.street_name(),
            "huisnummer": self.fake.building_number(),
            "postcode": self.fake.postcode().replace(" ", ""),
            "plaatsnaam": self.fake.city()
        }
    