from datetime import datetime
from faker import Faker
import random

class AccountGenerator:
    def __init__(self, fake: Faker):
        self.fake = fake

    def generate(self, name: str):
        return {
            "uuid": self.fake.uuid4(),
            "rekeninghouder": self.__format_string(name),
            "iban": self.fake.iban(),
        }
    
    def __format_string(self, input_string):
        if len(input_string) > 100:
            return input_string[:97] + '...'
        return input_string
